package cn.jiangzeyin.init;

import cn.jiangzeyin.common.PreLoadClass;
import cn.jiangzeyin.common.PreLoadMethod;

/**
 * Created by jiangzeyin on 2018/9/25.
 */
@PreLoadClass
public class InitClass {

    @PreLoadMethod
    private static void init() {
        //ProxyUtil.init();
    }
}
