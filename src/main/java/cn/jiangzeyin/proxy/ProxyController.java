package cn.jiangzeyin.proxy;

import cn.jiangzeyin.common.JsonMessage;
import cn.jiangzeyin.controller.base.AbstractController;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jiangzeyin on 2018/9/25.
 *
 * @author jiangzeyin
 */
@RestController
@RequestMapping("proxy")
public class ProxyController extends AbstractController {

    @RequestMapping(value = "showCache", produces = MediaType.TEXT_HTML_VALUE)
    public String showCache() {
        return JsonMessage.getString(200, "ok", ProxyUtil.getSize());
    }

    @RequestMapping(value = "showIp", produces = MediaType.TEXT_HTML_VALUE)
    public String showIp() {
        return getIp();
    }

    @RequestMapping(value = "getIp", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getIpItem() {
//        boolean check = ProxyUtil.checkWhite(getIp());
//        if (!check) {
//            return JsonMessage.getString(405, "非法访问");
//        }
        JSONObject info = ProxyUtil.next();
        if (info == null) {
            return JsonMessage.getString(404, "暂时没有可以ip");
        }
        return JsonMessage.getString(200, "ok", info);
    }
}
