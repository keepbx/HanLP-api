package cn.jiangzeyin;

import cn.jiangzeyin.common.ApplicationBuilder;
import cn.jiangzeyin.common.EnableCommonBoot;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * Created by jiangzeyin on 2017/12/9.
 *
 * @author jiangzeyin
 */
@SpringBootApplication
@ServletComponentScan
@EnableCommonBoot
public class HanLPApiApplication {
    /**
     * 启动执行
     *
     * @param args 启动参数
     */
    public static void main(String[] args) throws Exception {
        ApplicationBuilder.createBuilder(HanLPApiApplication.class).run(args);
    }
}
