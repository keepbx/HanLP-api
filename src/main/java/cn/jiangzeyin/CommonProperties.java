package cn.jiangzeyin;

/**
 * Created by jiangzeyin on 2017/12/9.
 */
public class CommonProperties {
    // 缓存前缀
    public static final String CACHE_PREFIX = "hanlp_api";
    private static final String HANLP = "hanlp";
    private static final String CLASSIFIERS = HANLP + ".classifiers";
    // 分类词典
    public static final String CORPUS_FOLDER = CLASSIFIERS + ".corpusFolder";
    // 训练好的模型存放文件夹
    public static final String MODEL_FOLDER = CLASSIFIERS + ".modelFolder";
}
