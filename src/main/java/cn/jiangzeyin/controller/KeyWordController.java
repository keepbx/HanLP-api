package cn.jiangzeyin.controller;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HtmlUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.hankcs.hanlp.HanLP;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * @author jiangzeyin
 * date 2017/12/9
 */
@RestController
@RequestMapping("keyword")
public class KeyWordController {
    /**
     * 获取关键字
     *
     * @param content 内容
     * @return json
     */
    @RequestMapping(value = "extract.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String keyWord(String content, String size, String isHtml) {
        if (StrUtil.isEmpty(content)) {
            return JsonMessage.getString(400, "content is null");
        }
        int sizeInt = Convert.toInt(size, 5);
        if (sizeInt < 1) {
            return JsonMessage.getString(400, "size must >=1");
        }
        boolean isHtmlBoolean = Boolean.valueOf(isHtml);
        if (!isHtmlBoolean) {
            content = HtmlUtil.unescape(content);
            content = HtmlUtil.cleanHtmlTag(content);
        }
        List<String> keywordList = HanLP.extractKeyword(content, sizeInt);
        return JsonMessage.getString(200, "success", keywordList);
    }
}
