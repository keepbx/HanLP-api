package cn.jiangzeyin.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jiangzeyin on 2017/12/9.
 */
@RestController
public class IndexController {
    /**
     * 首页
     *
     * @return 显示的字
     */
    @RequestMapping(value = {"index.html", "/"})
    public String index() {
        return "What do you look at?";
    }
}
