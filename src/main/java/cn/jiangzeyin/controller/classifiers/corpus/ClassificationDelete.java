package cn.jiangzeyin.controller.classifiers.corpus;

import cn.hutool.core.io.FileUtil;
import cn.jiangzeyin.common.JsonMessage;
import cn.jiangzeyin.controller.classifiers.BaseClassification;
import org.springframework.http.MediaType;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

/**
 * Created by jiangzeyin on 2017/12/9.
 */
@RestController
@RequestMapping("classifiers/corpus")
public class ClassificationDelete extends BaseClassification {
    /**
     * 删除文档
     *
     * @param category   分类
     * @param documentId 文件id
     * @return json
     */
    @RequestMapping(value = "del_document.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String del(String category, String documentId) {
        String tip = doCorpusFolder();
        if (tip != null) {
            return tip;
        }
        category = replacePath(category);
        documentId = replacePath(documentId);
        String path = FileUtil.normalize(corpusFolder + "/" + category + "/" + documentId + ".txt");
        File file = new File(path);
        if (file.exists()) {
            if (!file.isFile()) {
                return JsonMessage.getString(500, documentId + " not file");
            }
            if (!file.delete()) {
                return JsonMessage.getString(500, "delete failure");
            }
        }
        return JsonMessage.getString(200, "success");
    }

    /**
     * 删除分类
     *
     * @param category 分类名称
     * @return json
     */
    @RequestMapping(value = "del_category.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String category(String category) {
        String tip = doCorpusFolder();
        if (tip != null) {
            return tip;
        }
        category = replacePath(category);
        String path = FileUtil.normalize(corpusFolder + "/" + category);
        File file = new File(path);
        if (file.exists()) {
            if (!file.isDirectory()) {
                return JsonMessage.getString(500, category + " not directory");
            }
            if (!FileSystemUtils.deleteRecursively(file)) {
                return JsonMessage.getString(500, "delete failure");
            }
        }
        return JsonMessage.getString(200, "success");
    }

    /**
     * 清空所有分类
     *
     * @return json
     */
    @RequestMapping(value = "clear.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String clear() {
        String tip = doCorpusFolder();
        if (tip != null) {
            return tip;
        }
        File file = new File(corpusFolder);
        if (file.exists()) {
            if (!FileSystemUtils.deleteRecursively(file)) {
                return JsonMessage.getString(500, "clear failure");
            }
        }
        return JsonMessage.getString(200, "success");
    }
}
