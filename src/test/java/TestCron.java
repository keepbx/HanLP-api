import cn.hutool.core.date.DateUtil;
import cn.hutool.cron.CronUtil;

/**
 * Created by jiangzeyin on 2018/9/25.
 */
public class TestCron {
    public static void main(String[] args) {
//        System.out.println(\s);
        CronUtil.setMatchSecond(true);
        CronUtil.schedule("0/7 * * * * ?", (Runnable) () -> {
            System.out.println(DateUtil.date());
        });
        CronUtil.start();
    }
}
