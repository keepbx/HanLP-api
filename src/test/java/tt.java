//import com.hankcs.hanlp.HanLP;
//import com.hankcs.hanlp.corpus.tag.Nature;
//import com.hankcs.hanlp.dictionary.CustomDictionary;
//import com.hankcs.hanlp.dictionary.stopword.CoreStopWordDictionary;
//import com.hankcs.hanlp.seg.CRF.CRFSegment;
//import com.hankcs.hanlp.seg.Segment;
//import com.hankcs.hanlp.seg.common.Term;
//import com.hankcs.hanlp.tokenizer.BasicTokenizer;
//import com.hankcs.hanlp.tokenizer.NotionalTokenizer;
//
//import java.util.List;
//
///**
// * Created by jiangzeyin on 2017/12/9.
// */
//public class tt {
//    public static void main(String[] args) {
//        String text = "小区居民有的反对喂养流浪猫，而有的居民却赞成喂养这些小宝贝";
//        CoreStopWordDictionary.add("居民");
//        System.out.println(NotionalTokenizer.segment(text));
//        CoreStopWordDictionary.remove("居民");
//        System.out.println(NotionalTokenizer.segment(text));
//        List<Term> termList = BasicTokenizer.segment(text);
//        System.out.println(termList);
//        CoreStopWordDictionary.apply(termList);
//        System.out.println(termList);
//        CoreStopWordDictionary.FILTER = (term) -> {
//            switch (term.nature) {
//                case Nature.nz:
//                    System.out.println(term.word);
//                    return !CoreStopWordDictionary.contains(term.word);
//                default:
//                    return false;
//            }
//        };
//        System.out.println(NotionalTokenizer.segment(text));
//        String[] testCase = new String[]{"蓝翔给宁夏固原市彭阳县红河镇黑牛沟村捐赠了挖掘机"};
//        Segment segment = HanLP.newSegment().enablePlaceRecognize(true);
//        String[] sentenceArray = testCase;
//        int i = testCase.length;
//
//        int var7;
//        for (var7 = 0; var7 < i; ++var7) {
//            String sentence = sentenceArray[var7];
//            termList = segment.seg(sentence);
//            System.out.println(termList);
//        }
//
//        HanLP.Config.ShowTermNature = false;
//        //HanLP.Config.CRFSegmentModelPath = "D:\\hanlp\\model\\segment\\CRFSegmentModel.txt";
//        CustomDictionary.add("乐视");
//        segment = (new CRFSegment()).enableCustomDictionary(true);
//        sentenceArray = new String[]{"HanLP是由一系列模型与算法组成的Java工具包，目标是普及自然语言处理在生产环境中的应用。", "鐵桿部隊憤怒情緒集結 馬英九腹背受敵", "馬英九回應連勝文“丐幫說”：稱黨內同志談話應謹慎", "高锰酸钾，强氧化剂，紫红色晶体，可溶于水，遇乙醇即被还原。常用作消毒剂、水净化剂、氧化剂、漂白剂、毒气吸收剂、二氧化碳精制剂等。", "《夜晚的骰子》通过描述浅草的舞女在暗夜中扔骰子的情景,寄托了作者对庶民生活区的情感", "这个像是真的[委屈]前面那个打扮太江户了，一点不上品...@hankcs", "鼎泰丰的小笼一点味道也没有...每样都淡淡的...淡淡的，哪有食堂2A的好次", "克里斯蒂娜·克罗尔说：不，我不是虎妈。我全家都热爱音乐，我也鼓励他们这么做。", "今日APPS：Sago Mini Toolbox培养孩子动手能力", "财政部副部长王保安调任国家统计局党组书记", "2.34米男子娶1.53米女粉丝 称夫妻生活没问题", "你看过穆赫兰道吗", "国办发布网络提速降费十四条指导意见 鼓励流量不清零", "乐视超级手机能否承载贾布斯的生态梦"};
//        String[] var10 = sentenceArray;
//        var7 = sentenceArray.length;
//
//        for (int var11 = 0; var11 < var7; ++var11) {
//            String sentence = var10[var11];
//            termList = segment.seg(sentence);
//            System.out.println(termList);
//        }
//
//        for (i = 0; i < 5; ++i) {
//            new CRFSegment();
//        }
//
//    }
//}
