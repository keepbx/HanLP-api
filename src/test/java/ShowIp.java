import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

/**
 * Created by jiangzeyin on 2018/9/25.
 */
public class ShowIp {
    public static void main(String[] args) {
        for (int i = 0; i < 50; i++) {
            String json = HttpUtil.post("http://word-robot.ztoutiao.cn/proxy/getIp", (Map<String, Object>) null);
            JSONObject jsonObject = JSONObject.parseObject(json);
            if (jsonObject.getIntValue("code") == 200) {
                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                String ip = jsonObject1.getString("Ip");
                if (StrUtil.isEmpty(ip)) {
                    continue;
                }
                int port = jsonObject1.getIntValue("Port");
                if (port <= 0) {
                    continue;
                }
                try {
                    test(ip, port);
                } catch (Exception ignored) {

                }
            }
        }
    }

    private static void test(String ip, int port) throws IOException {
        //Proxy类代理方法
        URL url = new URL("http://word-robot.ztoutiao.cn/proxy/showIp");
        // 创建代理服务器
        InetSocketAddress addr = new InetSocketAddress(ip, port);
        // http 代理
        Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);
        URLConnection conn = url.openConnection(proxy);
        InputStream in = conn.getInputStream();
        String s = IoUtil.read(in, CharsetUtil.UTF_8);
        System.out.println("代理ip:" + ip + " 结果ip:" + s);
    }
}
