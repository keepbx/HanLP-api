import cn.hutool.core.util.ZipUtil;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

/**
 * Created by jiangzeyin on 2018/8/21.
 */
public class UN {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String s = "æµ\u008Bè¯\u0095ç¬¬ä¸\u0089æ\u0096¹ä¸\u0089ç\u009A\u0084";
        System.out.println(autoToUtf8(s));
        //
        s = "测试第三方三的";
        System.out.println(autoToUtf8(s));

        System.out.println(new String(s.getBytes("UTF-8"), "ISO-8859-1"));

        s = "aaa";
        System.out.println(autoToUtf8(s));

        System.out.println(new String(s.getBytes("UTF-8"), "ISO-8859-1"));


        s = "ÌáÈ¡ËÙ¶È¹ý¿ì£¡5ÃëÄÚÖ»ÄÜÌáÈ¡Ò»´Î.";
        byte[] bytes = ZipUtil.unGzip(s.getBytes());
        System.out.println(new String(bytes));
        System.out.println(autoToUtf8(s));
    }

    public static String autoToUtf8(String str) {
        if (null == str) {
            return null;
        }
        String newStr = new String(str.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        if (str.length() == newStr.length())
            return str;
        return newStr;
    }
}
